/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Bancomat {

    private int saldo_attuale = 0;
    private int massimo_prelev=0;
    private String Movimenti = "Lista movimenti: \n"+Adesso() +" APERTURA CONTO\n";

    Bancomat(int denaro, int max) {
        saldo_attuale = denaro;
        massimo_prelev=max;
    }

    int Visualizza_saldo() {
        return saldo_attuale;
    }

    String Visualizza_movimenti() {
        return Movimenti;
    }

    boolean Preleva(int denaro) {
        if ((denaro <= saldo_attuale)&&(denaro<=massimo_prelev)) {
            saldo_attuale = saldo_attuale - denaro;
            Movimenti += Adesso() + " PRELIEVO " + denaro + " �\n";
            massimo_prelev=massimo_prelev-denaro;
            return true;
        } else {
            return false;
        }
    }

    String Adesso() {
        
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int hour = cal.get(Calendar.HOUR);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        
        
        String data_e_ora_attuale = "[" + day + "/" + month + "/" + year + " - "+new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime())+"]";
        return data_e_ora_attuale;
    }
}

//guidanorberti