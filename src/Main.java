import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Main {

    static Bancomat B1;
    static JLabel scrittaSaldo;
    static JTextArea scrittaMovimenti;

    public static void main(String[] args) {

        B1 = new Bancomat(1000000, 250);

        JFrame frame = new JFrame();
        frame.setLayout(null);
        frame.setTitle("SIMULATORE Bancomat - Guida - Norberti 4BI");
        frame.setSize(500, 500);
        frame.setVisible(true);

        scrittaSaldo = new JLabel("Attualmente sul conto: " + B1.Visualizza_saldo() + " �", JLabel.CENTER);
        scrittaSaldo.setBounds(0, 0, 500, 40);

        scrittaMovimenti = new JTextArea(B1.Visualizza_movimenti());
        scrittaMovimenti.setBounds(0, 42, 500, 300);
        scrittaMovimenti.setLineWrap(true);
        scrittaMovimenti.setEditable(false);
        scrittaMovimenti.setAutoscrolls(true);

        frame.add(scrittaSaldo);
        frame.add(scrittaMovimenti);

        JTextField importo = new JTextField();
        importo.setBounds(0, 345, 150, 20);
        frame.add(importo);

        JButton prelevaa = new JButton("Preleva");
        prelevaa.setBounds(151, 345, 50, 20);
        prelevaa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EseguiPrelievo(importo.getText());
                importo.setText("");
            }
        });
        frame.add(prelevaa);
        
        JLabel defaulta = new JLabel("DEFAULT SALDO: 1000000, DEFAULT MASSIMO PREL: 250", JLabel.CENTER);
        defaulta.setBounds(0, 368, 500, 40);
        frame.add(defaulta);
        
        frame.setResizable(false);

    }

    static void EseguiPrelievo(String importo) {
        if ((importo != "") && (importo != null)) {
            int t_importo = Integer.parseInt(importo);
            if (t_importo > 0) {
                B1.Preleva(t_importo);
                scrittaSaldo.setText("Attualmente sul conto: " + B1.Visualizza_saldo() + " �");
                scrittaMovimenti.setText(B1.Visualizza_movimenti());
            }

        }

    }
}

//guidanorberti